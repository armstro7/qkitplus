import h5py
import hdf5pickle as hpkl
import os
import time
from pandas import *
from tables import *
import sys

def pickleObject(object,fileName):
    #object - The object(usually driver) that is being saved to database.
    #fileName - Name of the file to save to.
    #filePath - Path of the sweep to save picklejar to.

    visaTake = False
    stolenVisa = ""
    try:
        stolenVisa = object._visainstrument
        object._visainstrument = "Visa token"
        visaTake = True
    except:
        pass

    lockTake = False
    stolenLock = ""
    try:
        stolenLock = object.Instrument._access_lock
        object.Instrument._access_lock = "Lock token"
        lockTake = True
    except:
        print "WAH"
        pass


    hpkl.dump(object,'localcopy.hdf5') #Puts the object into the file
    time.sleep(1) #Sleep is neccessary as 'localcopy.hdf5' doesn't like it if yu try to read from it straight after dumping object to it.
    fs = h5py.File('localcopy.hdf5', 'r') #Open files and assign to variables.
    fd = h5py.File(fileName,'a')
    if fd.__contains__("entry/PickleJar") == False: #Check if the 'PickleJar' group exists in the current directory. If not, create it.
        fd.create_group("entry/PickleJar")
    fs.copy('data',fd.get("entry/PickleJar"),name=object.name)#Copies the object from the 'localcopy.hdf5' and saves it to the PickleJar in the filepath specified.
    fs.close() #Closefiles - always do this to avoid leaving files open and breaking the program.
    fd.close()

    os.remove('localcopy.hdf5') #Remove the local copy file from the system as it is not needed anymore

    if visaTake:
        object._visainstrument = stolenVisa
        print "visa retaken"


def pklFromHDF5(fileName,filePathOfPickleJar,objname):
    #fileName - Name of file that oject is read from
    #filePathOfPickleJar - file path of the picklejar in HDF5 database. If this is not specified then the program will break.
    #objname - Name of the object that the user wishes to read and return.



    fs = h5py.File(fileName, 'r') #Open files
    fd = h5py.File('localObject.hdf5','w')
    if fs.__contains__(filePathOfPickleJar+'/' + objname) == False: #Check if the object exists, if it doesn't, break the program.
        print 'The object "' +  objname + '" does not exist in the file "' + fileName + '"'
        sys.exit()
    fs.get(filePathOfPickleJar).copy(objname,fd,name='data')
    e = hpkl.load('localObject.hdf5') #Loads if it's a list
    fd.close()
    os.remove('localObject.hdf5')
    return e

class banan(object):
    def __init__(self,name):
        self.name = name
        #self.func = lambda x: self.help(x)
        self.funcHelp()

    def funcHelp(self):
        setattr(self, 'get_%s' % self.name,  self.help)

    def help(self,str="help"):
        print str



obj = banan("What")

obj.get_What()

f = open_file("help.hdf5", mode='w')
f.create_group(f.root,"entry")

pickleObject(obj,"help.hdf5")


second = pklFromHDF5("help.hdf5","entry/PickleJar","What")

second.help()
print "Name",second.name
second.funcHelp()
second.get_hmm()

f.close()
