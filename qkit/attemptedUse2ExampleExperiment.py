import qkit
from qkit.core.lib import calltimer
from qkit.instruments import SR_830
import numpy
import time
import qkit.measure.samples_class as sc
from qkit.gui.plot import plot
import datetime

from pprint import pprint
#from SR_830 import *
from qkit.storage import store

qkit.cfg['datadir'] = r'C:\Users\armstro7\PycharmProjects\qkit\qkit'
qkit.cfg['run_id'] = 'Run0'
qkit.cfg['user'] = 'qkit_user'
qkit.cfg['load_visa'] = True


qkit.start()

pprint(qkit.cfg)

#DEV1 = qkit.instruments.create('agilent_33210A', 'agilent_33210A', address="GPIB1::4::INSTR")
DEV2 = qkit.instruments.create('Keithley_2000','Keithley_2000GPIB', address = "GPIB1::24::INSTR")
#DEV2 = qkit.instruments.create('sdfgsfgdh','afhsgrza', address = "GPIB1::24::INSTR")


for j in range(10):
    h5d = store.Data(name = "data", mode = "a")
    print h5d.get_filepath()
    #a_co = h5d.add_value_vector('Frequency', unit = "Hz",   comment = "VNA frequency scan")
    b_co = h5d.add_value_vector("Voltage", unit = "V", comment = "comments")
    step = h5d.add_value_vector("time", unit = "s", comment = "comments")

    #stepF = h5d.add_view("Frequency vs time",x=step, y=a_co)
    stepV = h5d.add_view("Voltage vs time",x=step, y=b_co)
    #VvsF = h5d.add_view("Voltage vs frequency",x=a_co,y=b_co)

    plot.plot(h5d.get_filepath())
    DEV2.reset()
    DEV2.setState("Voltage")
    for i in range(100):
        #DEV1.setFrequency(i*100)
        time.sleep(0.5)
        print DEV2.getCurrentValue()
        b_co.append(DEV2.getCurrentValue())
        step.append(i)

