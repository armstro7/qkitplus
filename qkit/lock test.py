import thread
import time

# Define a function for the thread
def print_time(threadName, delay):
   count = 0
   if threadName == "Thread-1":
       file1 = open("test.txt","w+")
       file1.write("What up fool")
       file1.flush()
       print "File 1 done"
       while 1:
           pass
   else:
       time.sleep(2)
       file2 = open("test.txt","r")
       print "Trying"
       print file2.readline()

# Create two threads as follows
try:
   thread.start_new_thread( print_time, ("Thread-1", 2, ) )
   thread.start_new_thread( print_time, ("Thread-2", 4, ) )
except:
   print "Error: unable to start thread"

while 1:
   pass
