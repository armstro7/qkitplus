import qkit
from qkit.core.lib import calltimer
from qkit.instruments import SR_830
import numpy
import time
import qkit.measure.samples_class as sc
from qkit.gui.plot import plot

from pprint import pprint
#from SR_830 import *
from qkit.storage import store

qkit.cfg['datadir'] = r'C:\Users\armstro7\PycharmProjects\qkit\qkit'
qkit.cfg['run_id'] = 'Run0'
qkit.cfg['user'] = 'qkit_user'
qkit.cfg['load_visa'] = True


qkit.start()

pprint(qkit.cfg)

#DEV1 = qkit.instruments.create('agilent_33210A', 'agilent_33210A', address="GPIB1::4::INSTR")

h5d = store.Data(name = "data1", mode = "a")
print h5d.get_filepath()
a_co = h5d.add_value_vector('a', unit = "Hz",   comment = "VNA frequency scan")
b_co = h5d.add_value_vector("b", unit = "V", comment = "comments")
c_co = h5d.add_value_vector('c', unit = "Hz",   comment = "VNA frequency scan")
d_co = h5d.add_value_vector("d", unit = "V", comment = "comments")
e_co = h5d.add_value_vector('e', unit = "Hz",   comment = "VNA frequency scan")
f_co = h5d.add_value_vector("f", unit = "V", comment = "comments")
g_co = h5d.add_value_vector('g', unit = "Hz",   comment = "VNA frequency scan")
h_co = h5d.add_value_vector("h", unit = "V", comment = "comments")
i_co = h5d.add_value_vector('i', unit = "Hz",   comment = "VNA frequency scan")
j_co = h5d.add_value_vector("j", unit = "V", comment = "comments")
k_co = h5d.add_value_vector('k', unit = "Hz",   comment = "VNA frequency scan")
l_co = h5d.add_value_vector("l", unit = "V", comment = "comments")
m_co = h5d.add_value_vector('m', unit = "Hz",   comment = "VNA frequency scan")
n_co = h5d.add_value_vector("n", unit = "V", comment = "comments")
o_co = h5d.add_value_vector('o', unit = "Hz",   comment = "VNA frequency scan")
p_co = h5d.add_value_vector("p", unit = "V", comment = "comments")
q_co = h5d.add_value_vector('q', unit = "Hz",   comment = "VNA frequency scan")
r_co = h5d.add_value_vector("r", unit = "V", comment = "comments")
s_co = h5d.add_value_vector('s', unit = "Hz",   comment = "VNA frequency scan")
t_co = h5d.add_value_vector("t", unit = "V", comment = "comments")
u_co = h5d.add_value_vector('u', unit = "Hz",   comment = "VNA frequency scan")
v_co = h5d.add_value_vector("v", unit = "V", comment = "comments")
w_co = h5d.add_value_vector('w', unit = "Hz",   comment = "VNA frequency scan")
x_co = h5d.add_value_vector("x", unit = "V", comment = "comments")
y_co = h5d.add_value_vector('y', unit = "Hz",   comment = "VNA frequency scan")
z_co = h5d.add_value_vector("zpass", unit = "passes", comment = "comments")

timet = h5d.add_value_vector("time taken for pass", unit = "s", comment = "comments", x=z_co)

plot.plot(h5d.get_filepath())
for i in range(10000000):
    firstStart = time.time()
    a_co.append(i)
    b_co.append(i)
    c_co.append(i)
    d_co.append(i)
    e_co.append(i)
    f_co.append(i)
    g_co.append(i)
    h_co.append(i)
    i_co.append(i)
    j_co.append(i)
    k_co.append(i)
    l_co.append(i)
    m_co.append(i)
    n_co.append(i)
    o_co.append(i)
    p_co.append(i)
    q_co.append(i)
    r_co.append(i)
    s_co.append(i)
    t_co.append(i)
    u_co.append(i)
    v_co.append(i)
    w_co.append(i)
    x_co.append(i)
    y_co.append(i)
    z_co.append(i)
    timet.append(time.time()-firstStart)

while 1:
    pass
h5d.close()
