import qkit
from qkit.core.lib import calltimer
from qkit.instruments import SR_830
import numpy
import time
import qkit.measure.samples_class as sc
from qkit.gui.plot import plot
import datetime

from pprint import pprint
#from SR_830 import *
from qkit.storage import store

qkit.cfg['datadir'] = r'C:\Users\armstro7\PycharmProjects\qkit\qkit'
qkit.cfg['run_id'] = 'Run0'
qkit.cfg['user'] = 'qkit_user'
qkit.cfg['load_visa'] = True


qkit.start()

pprint(qkit.cfg)

#DEV1 = qkit.instruments.create('agilent_33210A', 'agilent_33210A', address="GPIB1::4::INSTR")
DEV2 = qkit.instruments.create('Keithley_2000','Keithley_2000GPIB', address = "GPIB1::24::INSTR")
#DEV2 = qkit.instruments.create('sdfgsfgdh','afhsgrza', address = "GPIB1::24::INSTR")


h5d = store.Data(name = "data", mode = "a")
for j in range(1):
    print h5d.get_filepath()
    print DEV2
    time.sleep(1)
    h5d.pickleObject(DEV2,h5d.get_filepath())
    #a_co = h5d.add_value_vector('Frequency', unit = "Hz",   comment = "VNA frequency scan")
    b_co = h5d.add_value_vector("Voltage", unit = "V", comment = "comments")
    step = h5d.add_value_vector("time", unit = "s", comment = "comments")

    plot.plot(h5d.get_filepath())

while 1:
    pass
DEV3 = h5d.pklFromHDF5(h5d.get_filepath(),"entry/PickleJar",DEV2.name)

print DEV3.states

DEV3.setState("Voltage")
print DEV3.state
