import qkit
from qkit.core.lib import calltimer
from qkit.instruments import SR_830
import numpy
import time
import qkit.measure.samples_class as sc
from qkit.gui.plot import plot
import datetime

from pprint import pprint
#from SR_830 import *
from qkit.storage import store

qkit.cfg['datadir'] = r'C:\Users\armstro7\PycharmProjects\qkit\qkit'
qkit.cfg['run_id'] = 'Run0'
qkit.cfg['user'] = 'qkit_user'
qkit.cfg['load_visa'] = True


qkit.start()

pprint(qkit.cfg)

DEV1 = qkit.instruments.create('agilent_33210A', 'agilent_33210A', address="GPIB1::4::INSTR")
DEV2 = qkit.instruments.create('Keithley_2000','Keithley_2000GPIB', address = "GPIB1::24::INSTR")
#DEV2 = qkit.instruments.create('sdfgsfgdh','afhsgrza', address = "GPIB1::24::INSTR")


h5d = store.Data(name = "data", mode = "a")
print h5d.get_filepath()
print DEV2
time.sleep(1)
h5d.pickleObject(DEV2,h5d)
#a_co = h5d.add_value_vector('Frequency', unit = "Hz",   comment = "VNA frequency scan")
b_co = h5d.add_value_vector("Voltage", unit = "V", comment = "comments")
step = h5d.add_value_vector("time", unit = "s", comment = "comments")
voltageVsTime = h5d.add_view("Voltage vs time",step,step,b_co)
DEV3 = h5d.pklFromHDF5(h5d,"entry/RealPickleJar",DEV2.name,visaAddress="GPIB1::24::INSTR")

print DEV3.states
'''s = ""
for parameter in DEV3._parameters:
    print parameter," = ", DEV3._parameters[parameter]
    for key in DEV3._parameters[parameter]:
        if key not in ["tags","get_func","set_func","flags"]:
            s+=parameter+" "+key+": "+str(DEV3._parameters[parameter][key])+"\n"
print s'''


plot.plot(h5d.get_filepath())
DEV1.set_default()
DEV1.set_output(1)
DEV3.reset()
DEV3.setState("Voltage")
print "DEV3.name = ",DEV3.name

begin = False
while not begin:
    print("Testing return value is valid")
    try:
        DEV3.getCurrentValue()
        begin=True
    except:
        pass

DEV3.onA()
time.sleep(1)
DEV3.do_set_voltageA(0.5)
time.sleep(1)
#print DEV3.do_get_voltageA()

for i in range(100):
    DEV1.set_amplitude((i+1)*0.01)
    time.sleep(3)
    print DEV3.getCurrentValue()
    b_co.append(DEV3.getCurrentValue())
    step.append(i)

while 1:
    pass
